package
{
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Bullet extends Sprite
	{
		private var mTexture:Texture;
		private var noImage:Image;
		private var tween:Tween;
		//public var gameOver:Boolean = false;
		
		public static const BULLET_CROSS:String = "bulletCross";
		
		[Embed(source="../assets/no.png")]
		public static const NoImage:Class;
		
		public function Bullet()
		{
			mTexture = Texture.fromBitmap(new NoImage());
			noImage = new Image(mTexture)
			addChild(noImage);
			
			tween = new Tween(this, 1.5);
			tween.animate("y", 0);
			tween.onComplete = onTweenComplete;
			tween.onCompleteArgs = [tween];
			Starling.juggler.add(tween);
			
			addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame)
		}
		
		private function onTweenComplete(tweenWontToDelete:Tween):void
		{
			if (parent != null)
			{
				parent.removeChild(this)
				Starling.juggler.removeTweens(tweenWontToDelete);
			}
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void
		{
			for (var i:int = 0; i < myData.brickArr.length; i++) // пробегаем все блоки
			{
				//if (crosses(myData.brickArr[i], this))
				if (crosses(myData.brickArr[i], this))
				{
					Starling.juggler.removeTweens(this); // удаляет все анимации свызаные с пулей
					if (parent)
					{
						parent.removeChild(this); //<<<<<<<<<<<
					}
					dispatchEvent(new bulletEvent(bulletEvent.CROSS, myData.brickArr[i], "bullet")); // передаем событие для удаления блока с которым пересеклась пуля
				}
			}
		}
		
		private function crosses(operand1:Image, operand2:Bullet):Boolean
		{
			if (operand2.y < (operand1.y - (myData.positionY + 19)) + operand1.height) // пересекает по y
			{
				if ((operand2.x + operand2.width > operand1.x) && (operand2.x < operand1.x + operand1.width)) // и пересекает по x
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
}