package
{
	//import flash.events.Event;
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class StarlingRoot extends Sprite
	{
		private var screen1:Screen1;
		private var screen2:Screen2;
		private var screen3:Screen3;
		private var screen4:Screen4;
		private var screen5:Screen5;
		
		public function StarlingRoot()
		{
			//screen1 = new Screen1();
			screen5 = new Screen5();
			addChild(screen5);
			//addChild(screen1);
			//screen1.addEventListener(Screen1.LANG_BUTTON_PRESS, onLangButtonPress)
		}
		
		private function onLangButtonPress(e:Event):void
		{
			myData.lang = screen1.value;
			trace("Data.lang=", myData.lang);
			screen1.removeEventListener(Screen1.LANG_BUTTON_PRESS, onLangButtonPress)
			removeChild(screen1);
			
			screen2 = new Screen2();
			addChild(screen2);
			screen2.addEventListener(Screen2.SCREEN_2_ENTER_PRESS, onScreen2enterPress)
		}
		
		private function onScreen2enterPress(e:Event):void
		{
			myData.nick = screen2.value;
			trace("Data.nick=", myData.nick);
			screen2.removeEventListener(Screen2.SCREEN_2_ENTER_PRESS, onScreen2enterPress)
			removeChild(screen2);
			
			screen3 = new Screen3();
			addChild(screen3);
			screen3.addEventListener(Screen3.PLAY_BTN_PRESS, onPlayBtnPress)
		}
		
		private function onPlayBtnPress(e:Event):void 
		{
			screen3.removeEventListener(Screen3.PLAY_BTN_PRESS, onPlayBtnPress)
			removeChild(screen3);
			
			screen4 = new Screen4();
			addChild(screen4);
			screen4.addEventListener(Screen4.LEVEL_BUTTON_PRESS, onLevelBtnPress)
		}	
		
		private function onLevelBtnPress(e:Event):void 
		{
			myData.difficultyLevel = screen4.value;
			trace("Data.difficultyLevel =", myData.difficultyLevel)
			
			screen4.removeEventListener(Screen4.LEVEL_BUTTON_PRESS, onLevelBtnPress)
			removeChild(screen4);
			screen5 = new Screen5();
			addChild(screen5);
		}
	}

}