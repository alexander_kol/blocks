package  
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import starling.display.Sprite;
	import feathers.controls.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Screen3 extends Sprite
	{
		public static const PLAY_BTN_PRESS:String = "playBtnPress";		
		
		[Embed(source="../assets/page3_1.jpg")]
		public static const BgImage1:Class;
		
		[Embed(source="../assets/page3_2.jpg")]
		public static const BgImage2:Class;
	
		[Embed(source="../assets/page3_btn.jpg")]
		public static const BtnImage:Class;
		
		private var myTimer:Timer;
		private var bgTexture:Texture
		private var bgImage1:Image
		private var bgImage2:Image
		private var btnImage:Image
		
		public function Screen3() 
		{
			bgTexture = Texture.fromBitmap(new BgImage1());
			bgImage1 = new Image(bgTexture);
			addChild(bgImage1);
			
			myTimer = new Timer(1000);
			myTimer.addEventListener(TimerEvent.TIMER, onTimer)
			myTimer.start();
		}
		
		private function onTimer(e:TimerEvent):void 
		{
			removeChild(bgImage1)
			myTimer.stop();
			
			bgTexture = Texture.fromBitmap(new BgImage2());
			bgImage2 = new Image(bgTexture);
			addChild(bgImage2);
			
			var quad1:Quad = new Quad(178, 182);
			quad1.alpha = 0.2;
			//addChild(quad1);
			
			var playBtn:Button = new Button;
			playBtn.defaultSkin = quad1;
			bgTexture = Texture.fromBitmap(new BtnImage());
			btnImage = new Image(bgTexture);
			playBtn.downSkin = btnImage;
			addChild(playBtn);
			playBtn.x = 448
			playBtn.y = 549
			playBtn.addEventListener(Event.TRIGGERED, onTrigged);
		}
		
		private function onTrigged(e:Event):void 
		{
			dispatchEvent(new Event(PLAY_BTN_PRESS));
		}		
		
	}

}