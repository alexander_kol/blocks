package
{
	import flash.events.AccelerometerEvent;
	import flash.events.TimerEvent;
	import flash.sensors.Accelerometer;
	import flash.utils.Timer;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Sprite;
	import feathers.controls.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Screen5 extends Sprite
	{
		[Embed(source="../assets/page5_bg.jpg")]
		public static const BgImage1:Class;
		
		[Embed(source="../assets/page5_tank.png")]
		public static const TankImage:Class;
		
		[Embed(source="../assets/no.png")]
		public static const NoImage:Class;
		
		private var mTexture:Texture;
		private var bgImage1:Image;
		private var noImage:Image;
		private var tankImage:Image;
		private var myTimer:Timer;
		private var shootTimer:Timer;
		private var timerValue:int = 0;
		private var timerText:TextField;
		private var accl:Accelerometer
		private var acclText:TextField;
		private var roundAccl:Number;
		private var container:Sprite;
		private var bullet:Bullet;
		private var brick:Brick;
		private var shoot:Boolean = true;
		private var explosion:Explosion;
		private var gameOver:Boolean = false;
		
		public function Screen5()
		{
			mTexture = Texture.fromBitmap(new BgImage1());
			bgImage1 = new Image(mTexture);
			addChild(bgImage1);
			
			container = new Sprite(); // в контейнере пуля, чтобы была на заднем фоне за танком
			addChild(container)
			
			mTexture = Texture.fromBitmap(new TankImage());
			tankImage = new Image(mTexture);
			myData.tankImage = tankImage;
			addChild(tankImage);
			tankImage.x = (1024 - tankImage.width) / 2;
			tankImage.y = 610;
			
			shootTimer = new Timer(100);
			shootTimer.addEventListener(TimerEvent.TIMER, onShootTimer)
			
			myTimer = new Timer(1000);
			myTimer.addEventListener(TimerEvent.TIMER, onTimer)
			myTimer.start();
			timerText = new TextField(150, 60, String(timerValue));
			addChild(timerText);
			timerText.x = 450;
			timerText.y = 700
			
			acclText = new TextField(450, 460, "accl");
			addChild(acclText);
			acclText.x = 450;
			acclText.y = 200
			
			if (Accelerometer.isSupported)
			{
				accl = new Accelerometer
				accl.setRequestedUpdateInterval(1);
				accl.addEventListener(AccelerometerEvent.UPDATE, onAcclUpdate)
			}
			else
			{
				acclText.text = "accelerometer not supported"
			}
			
			this.addEventListener(TouchEvent.TOUCH, onStageTrigged)
			
			brick = new Brick;
			addChild(brick);
			brick.createLine(2);
			
			this.addEventListener(KeyboardEvent.KEY_DOWN, onKeybord) //////////////////////////////////
			
			this.addEventListener(bulletEvent.CROSS, onBulletCross); // слушаем события от бонусного блока
			
			this.addEventListener('gameOver', onGameOver);
		}
		
		private function onGameOver(e:Event):void
		{
			gameOver = true;
		}
		
		private function onKeybord(e:KeyboardEvent):void //////////////////////////////////////////
		{
			if (e.charCode == 32)
			{
				brick.bonusBlock()
			}
			if (e.charCode == 113)
			{
				tankImage.x -= 10
			}
			else if (e.charCode == 119)
			{
				tankImage.x += 10
			}
		}
		
		private function onShootTimer(e:TimerEvent):void
		{
			shoot = true;
			shootTimer.stop();
		}
		
		private function onStageTrigged(e:TouchEvent):void
		{
			if (!gameOver)
			{
				var touch:Touch = e.getTouch(this)
				if (touch)
				{
					if ((String(touch.phase) == "began") && shoot)
					{
						shoot = false;
						shootTimer.start();
						
						bullet = new Bullet();
						bullet.y = 610;
						bullet.x = tankImage.x + 29;
						
						container.addChild(bullet);
						
						bullet.addEventListener(bulletEvent.CROSS, onBulletCross)
					}
				}
			}		
		}
		
		private function onBulletCross(e:bulletEvent):void
		{
			trace("onBelletCross", e.brick, e.sourse);
			
			for (var i:int = 0; i < myData.brickArr.length; i++) // удаляем из массива блок с которым пересеклась пуля и обновляем отображение блоков
			{
				if (myData.brickArr[i] == e.brick)
				{
					myData.brickArr.splice(i, 1);
				}
			}
			
			if (e.sourse == "bullet")
			{
				if (e.brick == myData.bonusImage)
				{
					Starling.juggler.removeTweens(e.brick); // убираем твин с бонусного блока
					
					trace('BONUS');
					explosion = new Explosion('bonus', e.brick.x, e.brick.y - 47 + (-myData.positionY)); //47 = 19*3 ширина блока // Если бонусных блоков более одного на экране - доработать
					addChild(explosion);
				}
				else
				{
					trace("пуля пересекла обычный блок");
					explosion = new Explosion('simple', e.brick.x, e.brick.y - 47 + (-myData.positionY)); //47 = 19*3 ширина блока
					addChild(explosion);
				}
			}
			brick.updateBlocks();
		}
		
		private function onAcclUpdate(e:AccelerometerEvent):void
		{
			roundAccl = parseFloat(e.accelerationX.toFixed(2));
			acclText.text = roundAccl.toString();
			
			if (roundAccl > 0)
			{
				tankImage.x -= roundAccl * 15;
				if (tankImage.x < 0)
				{
					tankImage.x = 1;
				}
			}
			else
			{
				tankImage.x += Math.abs(roundAccl) * 15;
				if (tankImage.x > 1024 - tankImage.width)
				{
					tankImage.x = 1023 - tankImage.width
				}
			}
		}
		
		private function onTimer(e:TimerEvent):void
		{
			timerValue++;
			timerText.text = String(timerValue)
			
			mString = "";
			var mString:String
			for (var i:int = 0; i < myData.brickArr.length; i++)
			{
				mString += String(myData.brickArr[i].x) + "/" + String(myData.brickArr[i].y) + " "
				acclText.text = mString
			}
		}
	}
}