package
{
	import feathers.controls.Radio;
	import feathers.core.ToggleGroup;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import starling.display.Sprite;
	import feathers.controls.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Screen4 extends Sprite
	{
		[Embed(source="../assets/page4.jpg")]
		public static const BgImage1:Class;
		
		[Embed(source="../assets/page4_hardBtn.jpg")]
		public static const BtnImage1:Class;
		
		[Embed(source="../assets/page4_middleBtn.jpg")]
		public static const BtnImage2:Class;
		
		[Embed(source="../assets/page4_simpleBtn.jpg")]
		public static const BtnImage3:Class;
		
		public static const LEVEL_BUTTON_PRESS:String = "levelButtonPress";
		
		private var _value:String;
		private var bgTexture:Texture
		private var bgImage1:Image
		private var btnImage1:Image
		private var btnImage2:Image
		private var btnImage3:Image
		
		public function Screen4()
		{
			bgTexture = Texture.fromBitmap(new BgImage1());
			bgImage1 = new Image(bgTexture);
			addChild(bgImage1);
			
			var quad1:Quad = new Quad(379, 89);
			quad1.alpha = 0.2;
			var quad2:Quad = new Quad(338, 89);
			quad2.alpha = 0.2;
			var quad3:Quad = new Quad(340, 99);
			quad3.alpha = 0.2;
			
			var btn1:Button = new Button;
			btn1.defaultSkin = quad1
			bgTexture = Texture.fromBitmap(new BtnImage1());
			btnImage1 = new Image(bgTexture);
			btn1.downSkin = btnImage1;
			addChild(btn1);
			btn1.x = 300
			btn1.y = 470
			btn1.addEventListener(Event.TRIGGERED, onTrigged1)
			
			var btn2:Button = new Button;
			btn2.defaultSkin = quad2
			bgTexture = Texture.fromBitmap(new BtnImage2());
			btnImage2 = new Image(bgTexture);
			btn2.downSkin = btnImage2;
			addChild(btn2);
			btn2.x = 300
			btn2.y = 370
			btn2.addEventListener(Event.TRIGGERED, onTrigged2)
			
			var btn3:Button = new Button;
			btn3.defaultSkin = quad3
			bgTexture = Texture.fromBitmap(new BtnImage3());
			btnImage3 = new Image(bgTexture);
			btn3.downSkin = btnImage3;
			addChild(btn3);
			btn3.x = 300
			btn3.y = 270
			btn3.addEventListener(Event.TRIGGERED, onTrigged3)
		
		}
		
		private function onTrigged1(e:Event):void
		{
			_value = "hard"
			dispatchEvent(new Event(LEVEL_BUTTON_PRESS))
		}
		
		private function onTrigged2(e:Event):void
		{
			_value = "middle"
			dispatchEvent(new Event(LEVEL_BUTTON_PRESS))
		}
		
		private function onTrigged3(e:Event):void
		{
			_value = "simple"
			dispatchEvent(new Event(LEVEL_BUTTON_PRESS))
		}
		
		public function get value():String
		{
			return _value;
		}
	
	}

}