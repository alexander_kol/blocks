package
{
	import starling.display.Image;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class bulletEvent extends Event
	{
		public static const CROSS:String = "cross";
		
		private var mBrick:Image
		private var mSourse:String
		
		public function bulletEvent(type:String, brick:Image, sourse:String, bubbles:Boolean = false)
		{
			super(type, bubbles);
			mBrick = brick;
			mSourse = sourse;
		}
		
		public function get brick():Image
		{
			return mBrick;
		}
		
		public function get sourse():String
		{
			return mSourse;
		}
	}
}