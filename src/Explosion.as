package
{
	import flash.display.MovieClip;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Explosion extends Sprite
	{
		[Embed(source="../assets/page5_explosion.png")]
		public static const ExplosionBulletImage:Class;
		
		[Embed(source="../assets/page5_explosion_bonus.png")]
		public static const ExplosionBulletBonusImage:Class;
		
		[Embed(source="../assets/page5_explosion_tank.png")]
		public static const ExplosionTankImage:Class;
		
		private var explosionBulletImage:Image;
		private var explosionBulletBonusImage:Image;
		private var explosionTankImage:Image;
		private var mTexture:Texture;
		private var explosionTimer:Timer;
		
		public function Explosion(type:String, x:Number, y:Number)
		{
			if (type == 'simple')
			{
				mTexture = Texture.fromBitmap(new ExplosionBulletImage());
				explosionBulletImage = new Image(mTexture);
				addChild(explosionBulletImage);
				explosionBulletImage.x = x;
				explosionBulletImage.y = y;
			}
			else if (type == 'bonus')
			{
				mTexture = Texture.fromBitmap(new ExplosionBulletBonusImage());
				explosionBulletBonusImage = new Image(mTexture);
				addChild(explosionBulletBonusImage);
				explosionBulletBonusImage.x = x;
				explosionBulletBonusImage.y = y;
			}
			else if (type == 'tank')
			{
				mTexture = Texture.fromBitmap(new ExplosionTankImage());
				explosionTankImage = new Image(mTexture);
				addChild(explosionTankImage);
				explosionTankImage.x = x;
				explosionTankImage.y = y;
			}
			
			explosionTimer = new Timer(1000);
			explosionTimer.addEventListener(TimerEvent.TIMER, onExplosionTimer);
			explosionTimer.start();
		}
		
		private function onExplosionTimer(e:TimerEvent):void
		{
			explosionTimer.stop();
			explosionTimer.removeEventListener(TimerEvent.TIMER, onExplosionTimer);
			
			if (parent) 
			{
				parent.removeChild(this);
			}			
		}
	}
}