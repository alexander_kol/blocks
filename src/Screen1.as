package
{
	import feathers.controls.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Screen1 extends Sprite
	{
		[Embed(source="../assets/lang_bg.jpg")]
		public static const BgImage:Class;
		
		[Embed(source="../assets/kaz_btn_pressed.jpg")]
		public static const KazPressedImage:Class;
		
		[Embed(source="../assets/rus_btn_pressed.jpg")]
		public static const RusPressedImage:Class;
		
		private var _value:String;
		
		public static const LANG_BUTTON_PRESS:String = "langButtonPress";
		
		public function Screen1()
		{
			var bgTexture:Texture = Texture.fromBitmap(new BgImage());
			var bgImage:Image = new Image(bgTexture);
			addChild(bgImage);
			
			var quad1:Quad = new Quad(79, 66);
			quad1.alpha = 0.2;
			var quad2:Quad = new Quad(79, 66);
			quad2.alpha = 0.2;
			
			var kazBtn:Button = new Button();
			kazBtn.defaultSkin = quad1;
			var kazBtnTexture:Texture = Texture.fromBitmap(new KazPressedImage());
			var kazBtnImage:Image = new Image(kazBtnTexture);
			kazBtn.downSkin = kazBtnImage;
			addChild(kazBtn);
			kazBtn.x = 100;
			kazBtn.y = 105;
			kazBtn.addEventListener(Event.TRIGGERED, onTrigged1);
			
			var rusBtn:Button = new Button();
			rusBtn.defaultSkin = quad2;
			var rusBtnTexture:Texture = Texture.fromBitmap(new RusPressedImage());
			var rusBtnImage:Image = new Image(rusBtnTexture);
			rusBtn.downSkin = rusBtnImage;
			addChild(rusBtn);
			rusBtn.x = 185;
			rusBtn.y = 105;			
			rusBtn.addEventListener(Event.TRIGGERED, onTrigged2);
		
		}
		
		private function onTrigged1(e:Event):void
		{
			_value = "kaz";
			dispatchEvent(new Event(LANG_BUTTON_PRESS));		
		}
		
		private function onTrigged2(e:Event):void
		{
			_value = "rus";
			dispatchEvent(new Event(LANG_BUTTON_PRESS));			
		}
		
		public function get value():String
		{
			return _value;
		}
	}

}