package
{
	import feathers.controls.Button;
	import feathers.controls.TextInput;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.textures.Texture;
	import flash.events.KeyboardEvent;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Screen2 extends Sprite
	{
		public static const SCREEN_2_ENTER_PRESS:String = "screen2enterPress";
		private var textImput:TextInput
		private var _value:String = "";
		
		[Embed(source="../assets/page2_bg.jpg")]
		public static const BgImage:Class;
		
		public function Screen2()
		{
			var bgTexture:Texture = Texture.fromBitmap(new BgImage());
			var bgImage:Image = new Image(bgTexture);
			addChild(bgImage);
			
			var quad1:Quad = new Quad(50, 50);
			quad1.alpha = 1;
			var quad2:Quad = new Quad(50, 50);
			quad2.alpha = 0.6;
			
			var btn1:Button = new Button;
			btn1.defaultSkin = quad1
			btn1.downSkin = quad2
			addChild(btn1);
			btn1.x = 820
			btn1.y = 370
			btn1.addEventListener(Event.TRIGGERED, onTrigged1)
			
			textImput = new TextInput();
			textImput.text = "";
			textImput.setFocus();
			textImput.width = 100;
			textImput.height = 20;
			textImput.x = 400
			textImput.y = 380
			addChild(textImput);			
			textImput.addEventListener(Event.CHANGE, onChange)
			//textImput.addEventListener(KeyboardEvent.KEY_UP, onKeyUp)
			//addEventListener(KeyboardEvent.KEY_UP, onKeyUp)
		
			//Starling.current.nativeStage.addEventListener(flash.events.KeyboardEvent.KEY_UP, onStageEnterPress)
		}
		
		private function onTrigged1(e:Event):void 
		{
			if (_value != "")
				{
					//trace("_value", _value);
					dispatchEvent(new Event(SCREEN_2_ENTER_PRESS));
				}
		}
		
	/*	private function onKeyUp(e:KeyboardEvent):void 
		{
			trace(e.charCode);
		}*/
		
		/*private function onStageEnterPress(e:flash.events.KeyboardEvent):void
		{
			//trace("stage");
			if (e.charCode == 13)
			{
				//trace("enter press");
				if (_value != "")
				{
					//trace("_value", _value);
					dispatchEvent(new Event(SCREEN_2_ENTER_PRESS));
				}
			}
		}*/
		
		private function onChange(e:Event):void
		{
			//trace(textImput.text);
			_value = textImput.text;
		}
		
		/*private function onKeyUp(e:KeyboardEvent):void
		{
			if (e.charCode == 13)
			{
				trace("enter press");
				if (textImput.text != "")
				{
					trace("textImput", textImput.text);
					_value = textImput.text;
					dispatchEvent(new Event(SCREEN_2_ENTER_PRESS));
				}
			}
		}*/
		
		
		
		
		public function get value():String
		{
			return _value;
		}
	}
}