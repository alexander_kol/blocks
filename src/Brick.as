package
{
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.events.EnterFrameEvent;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class Brick extends Sprite
	{
		[Embed(source="../assets/page5_brick.jpg")]
		public static const BrickImage:Class;
		
		private var mTexture:Texture;
		private var brickImage:Image;
		private var tween:Tween;
		private var bonusTween:Tween;
		private var positionX:Number = 0;
		private var mPositionY:Number;
		private var createLineTimer:Timer;
		private var blocksXArray:Array = [0, 61, 122, 183, 244, 305, 366, 427, 488, 549, 610, 671, 732, 793, 854, 915]
		private var bonusImage:Image;
		private var gameOver:Boolean = false;
		
		public function Brick()
		{
			createLineTimer = new Timer(1000)//////////////////////////
			createLineTimer.addEventListener(TimerEvent.TIMER, onCreateLineTimer)
			createLineTimer.start();
		
			//this.addEventListener('gameOver', onGameOver);
		}
		
		private function onCreateLineTimer(e:TimerEvent):void
		{
			createLine(1);
		}
		
		public function createLine(number:int):void
		{
			for (var j:int = 0; j < number; j++)
			{
				for (var i:int = 0; i < 16; i++) // 16 блоков в линии
				{
					mTexture = Texture.fromBitmap(new BrickImage());
					brickImage = new Image(mTexture)
					brickImage.x = i + positionX;
					brickImage.y = myData.positionY;
					addChild(brickImage);
					
					myData.brickArr.push(brickImage);
					
					positionX += brickImage.width
				}
				positionX = 0;
				
				myData.positionY -= 19
			}
			
			tween = new Tween(this, 1.2)
			tween.animate("y", Math.abs(myData.positionY) - 19)
			Starling.juggler.add(tween);
			
			for (var k:int = 0; k < myData.brickArr.length; k++) // проверяем на пересечение линии движения танка
			{
				var point:Point = localToGlobal(new Point(myData.brickArr[k].x, myData.brickArr[k].y))
				if (point.y > 570) 
				{
					var explosion:Explosion = new Explosion('tank', myData.tankImage.x - 30, myData.tankImage.y - 80); // 80, 30 на вскидку
					parent.addChild(explosion);
					
					this.dispatchEvent(new Event('gameOver', true));
					gameOver = true;
					createLineTimer.stop();
				}				
			}
		}
		
		public function updateBlocks():void
		{
			while (this.numChildren > 0) // очищаем
			{
				this.removeChildAt(0);
			}
			
			for (var i:int = 0; i < myData.brickArr.length; i++) // заполняем
			{
				addChild(myData.brickArr[i]);
			}
		}
		
		public function bonusBlock():void
		{
			if (!gameOver)
			{
				var randomXBlock:int = blocksXArray[Math.floor(Math.random() * 16)]
				var columnBlocks:Array = [];
				
				mPositionY = myData.positionY;
				
				for (var i:int = 0; i < myData.brickArr.length; i++)
				{
					if (myData.brickArr[i].x == randomXBlock)
					{
						columnBlocks.push(myData.brickArr[i])
					}
				}
				
				bonusImage = columnBlocks[0];
				for (var j:int = 0; j < columnBlocks.length; j++) // фильтруем массив, ищем самый нижний блок
				{
					if (columnBlocks[j].y > bonusImage.y)
					{
						bonusImage = columnBlocks[j];
					}
				}
				
				if (bonusImage) // если после фильтрации существует блок, а не пустое место
				{
					//trace('ready', bonusImage.x, bonusImage.y)
					myData.bonusImage = bonusImage;
					bonusTween = new Tween(bonusImage, 2);
					bonusTween.animate("y", 750 + myData.positionY); // 750 - на вскидку
					bonusTween.onComplete = onBonusTweenComplete;
					bonusTween.onCompleteArgs = [bonusImage]
					Starling.juggler.add(bonusTween);
					
					bonusImage.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
				}
				else
				{
					if (myData.brickArr.length != 0)
					{
						bonusBlock(); // рекуривно вызываем функцию
					}
					else
					{
						//trace("end blocks", myData.brickArr);
					}
				}
			}
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void
		{
			var point:Point = localToGlobal(new Point(bonusImage.x, bonusImage.y));
			if ((point.y + bonusImage.height > myData.tankImage.y) && (point.y < myData.tankImage.y + myData.tankImage.height)) // пересекает по Y
			{
				if ((point.x + bonusImage.width > myData.tankImage.x) && (point.x < myData.tankImage.x + myData.tankImage.width))
				{
					onBonusTweenComplete(bonusImage);
					var explosion:Explosion = new Explosion('tank', myData.tankImage.x - 30, myData.tankImage.y - 80); // 80, 30 на вскидку
					parent.addChild(explosion);
					
					this.dispatchEvent(new Event('gameOver', true));
					gameOver = true;
					createLineTimer.stop();
				}
			}
		}
		
		private function onBonusTweenComplete(bImage:Image):void
		{
			trace("onBonusTweenComplete", bImage);
			bImage.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
			this.dispatchEvent(new bulletEvent(bulletEvent.CROSS, bImage, "block", true)); // передаем событие для удаления блока, баблим его
		}
	}
}